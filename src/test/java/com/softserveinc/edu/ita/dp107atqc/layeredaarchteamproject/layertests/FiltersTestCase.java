package com.softserveinc.edu.ita.dp107atqc.layeredaarchteamproject.layertests;

import com.softserveinc.edu.ita.dp107atqc.layeredaarchteamproject.entities.Category;
import com.softserveinc.edu.ita.dp107atqc.layeredaarchteamproject.pages.MainPage;
import com.softserveinc.edu.ita.dp107atqc.layeredaarchteamproject.tests.domain.Specification;
import com.softserveinc.edu.ita.dp107atqc.layeredaarchteamproject.util.WebDriverUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by Sasha on 20.02.2017.
 */
public class FiltersTestCase {

    @BeforeClass
    public void setUp() {

        WebDriverUtils.getWebDriver();
        WebDriverUtils.OpenUrl("http://commentssprintone.azurewebsites.net");

    }

    @AfterClass
    public void tearDown() {
        WebDriverUtils.stop();
    }

    @Test
    public void TestCase1_UseFilterOnActive() {
        MainPage mainPage = new MainPage();

        Specification.get()
                .For(mainPage
                        .getFilterMenuPage()
                        .filterStatus("Active")
                        .applyButton()
                        .getCommentsTablePage()).allCommentsAreWithActiveStatus(true)
                .next()
                .check();

    }

    @Test
    public void TestCase2_UseFilterOnInactive() {
        MainPage mainPage = new MainPage();

        Specification.get()
                .For(mainPage
                        .getFilterMenuPage()
                        .filterStatus("Inactive")
                        .applyButton()
                        .getCommentsTablePage()).allCommentsAreWithActiveStatus(false)
                .next()
                .check();

    }

    @DataProvider
    public Object[][] categories() {
        return new Object[][]{
                {"Cat0"},
                {"Cat3"},
                {"Cat5"}
        };
    }

    @Test(dataProvider = "categories")
    public void TestCase3_UseFilterOnCategory(String category) {
        MainPage mainPage = new MainPage();

        Specification.get()
                .For(mainPage
                       .getFilterMenuPage()
                        .filterCategory(category)
                        .applyButton()
                        .getCommentsTablePage())
                .allCommentsAreWithCategory(new Category
                (category))
                .next()
                .check();
        mainPage.getFilterMenuPage().filterCategory("All");
        mainPage = mainPage.getFilterMenuPage().applyButton();

    }



}
