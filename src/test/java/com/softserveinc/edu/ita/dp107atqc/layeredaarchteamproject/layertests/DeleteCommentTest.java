package com.softserveinc.edu.ita.dp107atqc.layeredaarchteamproject.layertests;

import com.softserveinc.edu.ita.dp107atqc.layeredaarchteamproject.entities.Comment;
import com.softserveinc.edu.ita.dp107atqc.layeredaarchteamproject.pages.*;
import com.softserveinc.edu.ita.dp107atqc.layeredaarchteamproject.tests.domain.Specification;
import com.softserveinc.edu.ita.dp107atqc.layeredaarchteamproject.util.WebDriverUtils;
import org.openqa.selenium.WebDriverException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by Sasha on 21.02.2017.
 */
public class DeleteCommentTest {
    private Specification specification;
    MainPage mainPage;

    @BeforeClass
    public void setUp() {

        WebDriverUtils.getWebDriver();
        WebDriverUtils.OpenUrl("http://commentssprintone.azurewebsites.net");

    }

    @AfterClass
    public void tearDown() {
        WebDriverUtils.stop();
    }

    @Test
    public void TestCase1_DeleteCommentFromTable_thereIsNoCommentInTheTable() {
        specification = Specification.get();
        MainPage mainPage = new MainPage();
        CommentsTablePage commentsTablePage = mainPage.getCommentsTablePage();
        if (!(commentsTablePage.getPageNumber().equals("1"))) {
            commentsTablePage.setPageNumber("1");
        }

        Comment COMMENT_TO_DELETE = commentsTablePage
                .getCommentFromTableByNumber("1");
        commentsTablePage
                .selectCommentWithCommentNumber(COMMENT_TO_DELETE);
        MainMenuPage mainMenuPage = mainPage.getMainMenuPage();
        DialogPage dialogPage = mainMenuPage.clickOnDeleteButton();


        specification
                .For(dialogPage.textOnDialog())
                .valueIsMatched("Are you sure you want to delete the selected item(s)?")
                .next();

        mainPage = dialogPage.clickOnYesButton();

        commentsTablePage = mainPage.getCommentsTablePage();


        specification
                .For(mainPage.getInfoLabel())
                .isVisible()
                .valueIsMatched("Selected comments are deleted successfully")
                .next()
                .For(commentsTablePage)
                .isCommentNotExistsOnTheTable(COMMENT_TO_DELETE)
                .next()
                .check();

    }

    @Test
    public void TestCase2_StopDeletingCommentInDialogMenu_CommentExists() {
        MainPage mainPage = new MainPage();
        CommentsTablePage commentsTablePage = mainPage
                .getCommentsTablePage();
        if (!(commentsTablePage.getPageNumber().equals("1"))) {
            commentsTablePage.setPageNumber("1");
        }
        Comment NOT_DELETED_COMMENT = commentsTablePage
                .getCommentFromTableByNumber("2");
        commentsTablePage.selectCommentWithCommentNumber(NOT_DELETED_COMMENT);
        MainMenuPage mainMenuPage = mainPage.getMainMenuPage();
        mainPage = mainMenuPage
                .clickOnDeleteButton()
                .clickOnNoButton();

        commentsTablePage = mainPage
                .getCommentsTablePage()
                .deselectAllCommentsOnPage();

        Specification.get()
                .For(commentsTablePage)
                .isCommentExistsOnTheTable(NOT_DELETED_COMMENT)
                .next()
                .check();
    }

    @Test
    public void TestCase3_TryToDeleteCommentWithNoCheckingIt_getAlertMessage() throws InterruptedException {
        MainPage mainPage = new MainPage();
        CommentsTablePage commentsTablePage;
        if (mainPage.getCommentsTablePage().isAllCommAreUnchecked()) {
            commentsTablePage = mainPage.getCommentsTablePage();
        } else {
            commentsTablePage = mainPage.getCommentsTablePage().deselectAllCommentsOnPage();
        }
        if (!(commentsTablePage.getPageNumber().equals("1"))) {
            commentsTablePage.setPageNumber("1");
        }

        MainMenuPage mainMenuPage = mainPage.getMainMenuPage();
        try {
            mainMenuPage.clickOnDeleteButton();
        } catch (WebDriverException e) {

            AlertPage alert = new AlertPage();
            Specification spec = Specification.get()
                    .For(alert)
                    .compareText("Please, select one category")
                    .next();

            alert.clickOnAccept();
            spec.check();
        }
    }

    @Test
    public void TestCase4_DeleteCommentsFromTable_thereAreNoSuchCommentsInTheTable() {

        MainPage mainPage = new MainPage();
        CommentsTablePage commentsTablePage = mainPage.getCommentsTablePage();
        if (!(commentsTablePage.getPageNumber().equals("1"))) {
            commentsTablePage.setPageNumber("1");
        }

        Comment COMMENT_1_TO_DELETE = commentsTablePage
                .getCommentFromTableByNumber("3");
        Comment COMMENT2_TO_DELETE = commentsTablePage
                .getCommentFromTableByNumber("7");

        commentsTablePage.selectCommentWithCommentNumber(COMMENT_1_TO_DELETE);
        commentsTablePage.selectCommentWithCommentNumber(COMMENT2_TO_DELETE);

        MainMenuPage mainMenuPage = mainPage.getMainMenuPage();
        DialogPage dialogPage = mainMenuPage.clickOnDeleteButton();
        mainPage = dialogPage.clickOnYesButton();

        commentsTablePage = mainPage.getCommentsTablePage();

        Specification.get()
                .For(commentsTablePage)
                .isCommentNotExistsOnTheTable(COMMENT2_TO_DELETE)
                .next()
                .check();
        //mainPage.returnButton();

    }


}
