import java.math.BigDecimal;

/**
 * Created by stas on 2/7/17.
 */
public class Stas {
    public static void main(String[] args) {
        System.out.println(new Stas().getValue(1.1d, 1.2d));
    }

    public BigDecimal getValue(double v1, double v2) {
        return new BigDecimal(v1+"").add(new BigDecimal(v2+""));
    }
}
